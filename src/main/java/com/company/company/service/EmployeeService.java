package com.company.company.service;


import com.company.company.employee.Employee;
import com.company.company.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class EmployeeService {

    List<Employee> allEmployeeList = new ArrayList<>();


    @Autowired
    private UserRepo userRepo;

    public Map groupBloodGroup() {


        allEmployeeList = userRepo.findAll();

        Map<String, List<Employee>> groupByBloodGroupList = allEmployeeList.stream().
                collect(Collectors.groupingBy(check -> check.getBloodGroup()));

        return groupByBloodGroupList;

    }

    public List<Employee> getOverpaidEmployees() {


        allEmployeeList = userRepo.findAll();
        int value = 1;

        List<Employee> overPaidEmployeeList = allEmployeeList.stream().filter(check -> check.getAnnualCtc() / check.getExperience() > (value)).
                collect(Collectors.toList());
        return overPaidEmployeeList;

    }


    public List<Employee> getUnderpaidEmployees() {


        allEmployeeList = userRepo.findAll();
        int value = 1;

        List<Employee> underPaidEmployeeList = allEmployeeList.stream().filter(check -> check.getAnnualCtc() / check.getExperience() < (value)).
                collect(Collectors.toList());
        return underPaidEmployeeList;

    }


}
