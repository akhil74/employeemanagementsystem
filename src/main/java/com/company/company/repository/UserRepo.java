package com.company.company.repository;

import com.company.company.employee.Employee;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepo extends JpaRepository<Employee, Integer> {


}
