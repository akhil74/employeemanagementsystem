package com.company.company.controller;

import com.company.company.employee.Employee;
import com.company.company.service.EmployeeService;
import com.company.company.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private UserRepo userRepo;
    @Autowired
    private EmployeeService employeeService;


    @GetMapping()               //To fetch all the employee details

    public ResponseEntity<List<Employee>> getEmployees() {


        List<Employee> allEmployeeList = new ArrayList<>();
        allEmployeeList = userRepo.findAll();

        if (userRepo != null) {
            return ResponseEntity.status(HttpStatus.OK).body(allEmployeeList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping()                  //To add a new employee to DB
    public ResponseEntity<Object> saveEmployee(@RequestBody Employee employee) {

        Employee savedEmployee = userRepo.save(employee);

        if (savedEmployee != null) {

            return ResponseEntity.status(HttpStatus.CREATED).body(savedEmployee);
        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PutMapping("/{id}")             //To update an existing employee details
    public ResponseEntity<Object> updateEmployee(@PathVariable int id, @RequestBody Employee employee) {

        Employee updatedEmployee = userRepo.findById(id).get();
        updatedEmployee.setName(employee.getName());
        updatedEmployee.setAddress(employee.getAddress());
        updatedEmployee.setDesignation(employee.getDesignation());
        updatedEmployee.setContact(employee.getContact());
        updatedEmployee.setEmail(employee.getEmail());
        updatedEmployee.setAnnualCtc(employee.getAnnualCtc());
        updatedEmployee.setExperience(employee.getExperience());
        updatedEmployee.setBloodGroup(employee.getBloodGroup());

        Employee savedEmployee = userRepo.save(updatedEmployee);


        if (savedEmployee != null) {

            return ResponseEntity.status(HttpStatus.OK).body(savedEmployee);
        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @GetMapping("/groupbyblood")     //To group the employees based on bloodgroup
    public ResponseEntity<Map> groupBloodGroup() {


        Map<String, List<Employee>> bloodGroupMap = new HashMap<>();
        bloodGroupMap = employeeService.groupBloodGroup();

        if (bloodGroupMap.isEmpty()) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(bloodGroupMap);
        }


    }

    @GetMapping("/overpaid")            //To find overpaid employees
    public ResponseEntity<List<Employee>> getOverPaidEmployees() {

        List<Employee> allOverpaidEmployeeList = new ArrayList<>();
        allOverpaidEmployeeList = employeeService.getOverpaidEmployees();

        if (allOverpaidEmployeeList != null) {
            return ResponseEntity.status(HttpStatus.OK).body(allOverpaidEmployeeList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }


    }

    @GetMapping("/underpaid")      //To find underpaid employees
    public ResponseEntity<List<Employee>> getUnderPaidEmployees() {

        List<Employee> alUnderEmployeeList = new ArrayList<>();
        alUnderEmployeeList = employeeService.getUnderpaidEmployees();

        if (alUnderEmployeeList != null) {
            return ResponseEntity.status(HttpStatus.OK).body(alUnderEmployeeList);

        } else {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }


    }

}
